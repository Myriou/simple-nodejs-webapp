FROM node:14

# Définition du répertoire de travail dans l'image
WORKDIR /app

# Copie du fichier count-server.js dans l'image
COPY ./src/count-server.js /app/

# Exposition du port
EXPOSE 8080

# Commande pour démarrer l'application avec la variable d'environnement
CMD ["sh", "-c", "node count-server.js $APPLICATION_INSTANCE"]
