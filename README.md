# Simple NodeJS Webapp

The application uses an environment variable `APPLICATION_INSTANCE`.  
This variable will be used by the application to listen on a specific path (i.e. `/${application_instance}/health`).

## Launch the application:

```bash
export APPLICATION_INSTANCE=example
node src/count-server.js
```


### Partie 1 :

Docker and Containerization
a : voir dockerfile

b : docker build -t simple-nodejs-webapp ./
docker run -it -p 8080:8080 simple-nodejs-webapp

c: docker login
docker tag simple-nodejs-webapp myriou/simple-nodejs-webapp
docker push myriou/simple-nodejs-webapp


Kubernetes :  Kubernetes ne fonctionnant pas sur mon pc (pb de virtualization) j'ai réalisé les documents yaml en partant de la théorie et en essayant d'adapté au projet donné

a. voir fichier deployemnt.yaml (inspiration : https://k8s-examples.container-solutions.com/examples/Deployment/Deployment.html#simple-deployment.yaml)
b:
kubectl apply -f deployment.yaml
kubectl get deployments
kubectl expose deployment simple-nodejs-webapp-deployment --type=LoadBalancer --name=simple-nodejs-webapp-service --port=8080
kubectl get service simple-nodejs-webapp-service

c: fichier deployment.yaml : 5 replicas   
d : voir fichier service.yaml
kubectl apply -f service.yaml     (inspiration : https://k8s-examples.container-solutions.com/examples/Service/Service.html)
e: voir fichier deployment.yaml

### Partie 2 

• Q1: Define what is a Microservices architecture and how does it differ from a Monolithic application.

The Microservices architecture breaks down an application into small, independent services, each performing a specific function and communicating through well-defined interfaces. This differs from a monolithic application where all processes are tightly coupled and operate as a single entity.

• Q2: Compare Microservices and Monolithic architectures by listing their respective pros and cons.

Microservices architectures : 

Pros : Scalability(enables scaling individual services based on demand), Flexibility (easier to update and deploy), Ease of deployement (Microservices can be deployed and evolved independently)

Cons : Complexity (managing multiple microseervices can be complex  with monitoring), Data consistency (maintaining data consistency between microservice), Security ( needs to be addressed at every level (not everyone may have access to everything))

Monolithic Architecture 
Pros : Simplicity ( easier to develop especially for small projects), Direct Communication (components communicate directly with each other)

Cons : Limited Scalabilit (Deployment and scaling often require reproducing the entire application), Delicate Maintenance (update can be tricky, as a change may impact the entire application) and rigidity (less flexible to adopt new technologies or evolve specific parts)

• Q3: In a Micoservices architecture, explain how should the application be split and why.

Microservices should be split based on webteam capabilities to ensure each service is focused, independent, and can be developed and scaled autonomously.So like that each team can work independently

• Q4: Briefly explain the CAP theorem and its relevance to distributed systems and Microservices.
The CAP theorem states that in a distributed system, it's impossible to simultaneously provide Consistency, Availability, and Partition Tolerance. You can only provide 2 at the same time

• Q5: What consequences on the architecture ?
the consequence are with the choice in data storage and manage the balance between consistency and availability which impact the resilience and performance of Microservices

• Q6: Provide an example of how microservices can enhance scalability in a cloud environment.

It can enhance scalability by scaling individual services based on demand and so like that if their is lot of demand, more service will open and otherwise a little will be open

• Q7: What is statelessness and why is it important in microservices archi-tecture ?
It means that each request from a client contains all the information needed to fulfill that request. And its crucial because it allow scalability, fault tolerance and its simple because like that any service instance can handle every request.

• Q8: What purposes does an API Gateway serve ?
An API Gateway acts as a central entry point for managing and routing requests to various Microservices. It allow to handle authentification for example.